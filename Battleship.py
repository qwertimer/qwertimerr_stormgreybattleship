import csv

# class ship
class Ship:
    """A ship """
    # 
    def __init__( self, type,size ):
        self.type = type
        self.size=size
        self.row=0
        self.col=0
        
    def getsize(self):
        return self.size
    
    def setsize(self,size):
        self.size=size
    
    def getlocation(self):
        return self.row,self.col
    
    def setlocation(self,row,col):
        self.col = col
        self.row = row
    
    def getorientation(self):
        return self.orientation
    
    def setorientation(self,orientation):
        # TODO: assert either H or V
        self.orientation = orientation

        
class Player:
    """A Player"""

    def __init__(self, name, ldfilename):
        # Player owns a Fleetlaydown
        # player has a board - contains record of their Fleetlaydown and oponents attack
        self.name = name
        self.board = Board()
        # filename= "settings/"+name+ "laydown.txt"  #default version
        filename = ldfilename
        self.fleetlaydown = FleetLaydown(filename)
        ok=self.board.validate(self.fleetlaydown)
        print(self.name+ " laydown valid?: "+str(ok))
        # TODO: raise exception if laydown is not ok
        # assert(ok)

    def count_sunk(self):
        sunkcount = 0
        cells = self.board.cells
        for ship in self.fleetlaydown.ships:
            # check if sunk
            shotcount = 0
            if (ship.getorientation() == "H"):
                row = ship.row
                for col in range(ship.col, ship.col + ship.getsize()):
                    if (cells[row - 1][col - 1] == 3):
                        shotcount += 1
            elif (ship.getorientation() == "V"):
                col = ship.col
                for row in range(ship.row, ship.row + ship.getsize()):
                    if (cells[row - 1][col - 1] == 3):
                        shotcount += 1
            if (shotcount >= ship.getsize()):
                sunkcount += 1
        return (sunkcount)

    def fleet_size(self):
        return (self.fleetlaydown.ship_count())

    def attack(self, row, col):
        """Get result of a shot landing on my bosrd at this location"""
        return (self.board.shot_result(row, col))  # defer decision to board: did my force get hit?

class FleetLaydown:
    """FleetLaydown is a collection of ships"""
    def __init__(self, filename):
        # initialise data structure to hold laydown
        self.ships = []
        #initialise allowed fleet
        fleetconfig="settings/fleetconfig.txt"
        self.reference_fleet={}
        self.load_fleet(fleetconfig)
        #load laydown
        self.load_laydown(filename)

    def add_ship(self,shipclass, row,col, orientation):
        """Add a ship of given class to the laydown at specified location and orientation"""
        if shipclass in self.reference_fleet:  #if allowed type of ship
            #TODO: fully validate entries against what is allowed in the reference fleet
            #eg maxallowed=self.reference_fleet[shipclass][0]
            ship=Ship(shipclass, self.reference_fleet[shipclass][1])
            ship.setlocation(row,col)
            ship.setorientation(orientation)
            self.ships.append(ship)
        
    def get_ships(self):
        return self.ships

    def ship_count(self):
        return (len(self.ships))

    def load_fleet(self, filename):
        """load reference fleet data"""
        input_file = csv.DictReader(open(filename))
        self.reference_fleet={}
        for row in input_file:
            shipclass =row["class"]
            shipcount=int(row["count"])
            shipsize=int(row["size"])
            self.reference_fleet[shipclass]=(shipcount, shipsize)

    def load_laydown(self, filename):
        """load player's laydown from a file"""
        input_file = csv.DictReader(open(filename))
        self.ships = []
        for row in input_file:
            shipclass=row["shipname"]
            shiporientation=row["layout"]
            shiprow=int(row["row"])
            shipcol=int(row["column"])
            self.add_ship(shipclass, shiprow,shipcol,shiporientation)

        
class  Board:
    '''Each player has board - contains record of opponents attack in cells'''
    def __init__(self):
        configfile = "settings/gameconfig.txt"  #default size
        self.getParameters(configfile)
        self.reset_cells()

    def reset_cells(self):
        self.cells=[[0 for col in range(self.width)] for row in range(self.height)]
        # Peculiarity of Python lists
        # cells=[[row*col for col in range(10)] for row in range(5)]
        #print(cells[2][9]) #[row][col] notation, index base is 0

    def set_width(self,width):
        self.width = width
        self.reset_cells()
        
    def set_height(self,height):
        self.height = height
        self.reset_cells()

    def getParameters(self, filename):
        """Read filename to get board dimensions"""
        self.width=0
        self.height=0
        dict={}
        with open(filename, "r") as file_handle:
            # this will read line by line into memory
            for line in file_handle:
                # process line
                key, value = line.split(",")
                try:
                    dict[key] = value
                except ValueError as e:
                    print("Bad parameters: "+ key + " is not a valid key")
                continue
        self.width= int(dict['width'])
        self.height= int(dict['height'])

    def shot_result(self, row, col):
        #  Cell has value of 0 empty, 1 empty with shot, 2 occupied, 3 occupied and hit
            val=self.cells[row - 1][col - 1]
            if (val==0):
                newval=1
            elif (val==1):
                newval=1
            elif (val==2):
                newval=3
            elif (val==3):
                newval=3
            self.cells[row - 1][col - 1] = newval
            return(newval)

    def validate(self,laydown):
        """Read the laydown and determines if it is valid.
        Parameters
        laydown:    A laydown object.
        Return
        Valid cells array if valid, None otherwise"""
        fleet = laydown.ships
        cells =[[0 for col in range(self.width)] for row in range(self.height)]
        isvalid = True
        for ship in fleet:
            # check if any previous ship occupies this space
            # and mark position of this ship for subsequent checks
            if (ship.getorientation() == "H"):
                row = ship.row
                for col in range(ship.col, ship.col + ship.getsize()):
                    isvalid = (isvalid) and (cells[row - 1][col - 1] == 0) and (col <= self.width) and (col>0)
                    cells[row-1][col-1] = 2  # mark as occupied
            elif (ship.getorientation() == "V"):
                col = ship.col
                for row in range(ship.row, ship.row + ship.getsize()):
                    isvalid = (isvalid) and (cells[row - 1][col - 1] == 0) and (row <= self.height) and (row>0)
                    cells[row-1][col-1] = 2  # mark as occupied
        if isvalid:
            self.cells=cells  #copy valid game board
        return(isvalid)



class Game:
    """Game"""
    def __init__(self, blue, red):
        self.bluePlayer =blue
        self.redPlayer = red
        self.reset()
        self.symbol=[' ','0','#','*']

    def reset(self):
        """Return game to initial state"""
        self.gameover = False

    def readAttacks(self,attacksfile):
        #read in file using name above
        input_file = csv.DictReader(open(attacksfile))
        self.attacks = []
        i=0
        for row in input_file:
            shotrow=int(row["row"])
            shotcol=int(row["col"])
            i+=1
            if (i%2 ==0) :
                #blueplayer's turn
                player= self.bluePlayer
                opponent= self.redPlayer
            else:
                #redplayers turn
                player=self.redPlayer
                opponent= self.bluePlayer
            outcome, report = self.adjudicate(shotrow, shotcol, player, opponent)
            print(report)
            if self.gameover:
                print(opponent.name + " fleet all sunk. " + player.name + " player wins!")
                break


    def adjudicate(self, row, col, player, opponent):
        """Determine the outcome of a shot at row,col by player against opponent"""
        #is the opponent's board hit by shot at row, col?
        msg={1:"Miss", 3:"Hit", -1:"Sunk"}
        scount = opponent.count_sunk()
        outcome = opponent.attack(row, col)
        recount = opponent.count_sunk()
        if recount > scount:
            outcome = -1
            if (recount >= opponent.fleet_size()):
                self.gameover = True
        report = player.name + " Fired at: " + str(row) + "," + str(col) + " with outcome: " + msg[outcome]
        return (outcome, report )

    def displayBoard(self, board, hideShip=True):
        topBot = '--'
        for col in board.cells[0]:
        	topBot = topBot+'-'
        print(topBot)
        for row in board.cells:
            ln ='|'
            for col in row:
                sym = self.symbol[col]
                if (hideShip and col==2): 
                    sym = self.symbol[0]
                ln = ln + sym
            ln=ln+'|'
            print(ln)
        print(topBot)
        
    def displayOutcome(self):  # TODO: update with more meaningful messages
        """Report current state of a game"""
        print("Blue Player Board")
        self.displayBoard(self.bluePlayer.board, hideShip = False)
        print("Red Player Board")
        self.displayBoard(self.redPlayer.board, hideShip = False)
        if self.gameover:
            print("Game over")
        else:
            print("Game undecided")

    def play(self, attacksfile):
        """Play a game reading moves from attackfile and show the results"""
        self.reset()
        self.readAttacks(attacksfile)
        self.displayOutcome()

        

if __name__ == '__main__':
    # Player owns a Fleetlaydown
    # player has a board - contains record of their Fleetlaydown and opponents attack
    # laydown class is integrated into FleetLaydown
    # Blue player, red player as instances of player

    game = Game(Player("Blue", "settings/Bluelaydown.txt"), Player("Red", "settings/Redlaydown.txt"))
    game.play("settings/attacks.txt")
