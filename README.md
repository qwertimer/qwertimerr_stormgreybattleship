## Battleship Game for Team Storm Grey

This is a project for a short course in **Professional Software Engineering** run by University of Sydney School of Information Technologies for scientific staff at Defence Science and Technology Group.

This codebase is the effort of **Team Storm Grey** consisting of Andrew Donohoo, John Chisholm, Jason Barles.
We have an accompanying [Trello page](https://trello.com/b/4hyRVnjv/storm-grey-battleship) for our project.

---

## Project Description

In this project we will create a Python version of the classic game **Battleship**.

![Classic Battleship](http://sites.psu.edu/gameclassblog/wp-content/uploads/sites/32415/2015/09/17msrnrw3mpxjjpg.jpg)

## Game Details

The game is a guessing game for two players and is played on two grids. 
The game’s objective is to destroy the opponent’s fleet of ships which are located on a hidden grid, before the opponent destroy's your fleet of ships. 

Each player possesses their own grid and at the beginning of the game places their own fleet of ships. 
The locations of ships are concealed from the opponent player. 
Players take alternating turns to aim “shots” at each other’s ships by specifying a grid location on their opponent’s board. 
Each shot is labelled as a miss or a hit. 
If all fields that a ship occupies are hit, then it is sunk. 
The opponent reports the miss, hit, and sunk cases. 

A fleet consists of various ship classes of specific length. 
Each ship occupies a consecutive number of squares either horizontally or vertically. 
The type and number of ships for each player are the same. 
The grid size and the number of ships per class are configurable via a simple configuration file describing a fleet.

After setting up the grids for each player with their fleets, the first player begins firing a round at a specified location on the opponent’s grid. 
The opponent replies with either hit or miss. 
The first player tracks the hit/miss in an auxiliary grid to improve the guesses for the next shot. 
The second player continues to shoot onto the grid of the first player. 
The second player also has an auxiliary grid to record the hits/misses. 
If a ship of a player is sunk after a successful shot (i.e., all squares of a ship are shot), the player announces the ship as “sunk” to the opponent.

The game is won by the first player to destroy their opponent's entire fleet.

---

BTW a [Markdown Cheat Sheet](https://bitbucket.org/tutorials/markdowndemo) is really handy for these Readme files.
