import unittest
from Battleship import *

class BoardTest(unittest.TestCase):
    def setUp(self):
        self.board = Board()

    def test_validateOverlap(self):
        # Testing if laydown of ships overlap
        self.filename = "settings/laydownConfigOverlap.txt"
       # self.playerblue = Player("Blue", self.filename)
        self.laydown = FleetLaydown(self.filename)
        self.board.validate(self.laydown)
        self.assertEqual(self.board.validate(self.laydown), False)

    def test_validateOutsideRow(self):
        # Testing if laydown of ships go outside the grid row
        self.filename="settings/laydownConfigOutsideRow.txt"
        self.laydown = FleetLaydown(self.filename)
        self.board.validate(self.laydown)
        self.assertEqual(self.board.validate(self.laydown), False)

    def test_validateOusideColumn(self):
        # Testing if laydown of ships go outside the grid column
        self.filename="settings/laydownConfigOutsideColumn.txt"
        self.laydown = FleetLaydown(self.filename)
        self.board.validate(self.laydown)
        self.assertEqual(self.board.validate(self.laydown), False)

    def test_validateInvalidShipType(self):
        # Testing if laydown of ships has a ship type that is invalid
        self.filename="settings/laydownConfigInvalidShipType.txt"
        self.laydown = FleetLaydown(self.filename)
        self.board.validate(self.laydown)
        self.assertEqual(self.board.validate(self.laydown), False)

    def test_validateShipStartOKButEndsOutside(self):
        # Testing if laydown of ships has a ship that starts within the 
        # board, but ends up being outside the board
        self.filename="settings/laydownConfigShipStartOKButEndsOutside.txt"
        self.laydown = FleetLaydown(self.filename)
        self.board.validate(self.laydown)
        self.assertEqual(self.board.validate(self.laydown), False)

    def test_validateInvalidOrientation(self):
        # Testing if the laydown has a ship that contains a invalid Orientation type
        self.filename="settings/laydownConfigInvalidOrientation.txt"
        self.laydown = FleetLaydown(self.filename)
        self.board.validate(self.laydown)
        self.assertEqual(self.board.validate(self.laydown), False)

    def test_validateValid(self):
        # Testing that a valid laydown should return a successful result
        # assumes a 10 x 10 grid
        # assumes carrier length 5 and battleship length 4
        # cells = [ [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #           [0, 2, 2, 2, 2, 2, 0, 0, 0, 0],
        #           [0, 2, 0, 0, 0, 0, 0, 0, 0, 0],
        #           [0, 2, 0, 0, 0, 0, 0, 0, 0, 0],
        #           [0, 2, 0, 0, 0, 0, 0, 0, 0, 0],
        #           [0, 2, 0, 0, 0, 0, 0, 0, 0, 0],
        #           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        #           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

        self.filename="settings/laydownConfigValid.txt"
        self.laydown = FleetLaydown(self.filename)
        self.board.validate(self.laydown)
        self.assertEqual(self.board.validate(self.laydown), True)
        
if __name__=='__main__':
    unittest.main(argv=['first-arg-is-ignored', "BoardTest"],exit=False)


