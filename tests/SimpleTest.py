import unittest
from Battleship import *

class BoardTest(unittest.TestCase):
    def setUp(self):
        self.ship = Ship("Carrier",3)

    def test_validateSize(self):
        # Testing if laydown of ships overlap
        self.assertEqual(self.ship.getsize(), 3)
        
if __name__=='__main__':
    unittest.main(argv=['first-arg-is-ignored', "BoardTest"],exit=False)


