import unittest
import sys
from Battleship import *

class GameTest(unittest.TestCase):
    def setUp(self):
        self.playerblue = Player("Blue", "settings/gameConfigBlueLaydown.txt")
        self.playerred = Player("Red", "settings/gameConfigRedLaydown.txt")
        self.game = Game(self.playerblue, self.playerred)

#    def test_validateOutsideTopLeft(self):
        # Assumming board is 10 x 10 starting from 0
        # Testing if attack is on the outside of the board in the top left corner 
        # and thus negative numbers too
#        self.attackconfig="settings/attackConfigOutsideTopLeft.txt"

    def test_validateAttacksRedWin(self):
        # Testing the case where the red player wins and blue loses
        self.attackconfig="settings/gameConfigAttacksRedWin.txt"
        self.game.play(self.attackconfig)
        try:
            from StringIO import StringIO
        except ImportError:
            from io import StringIO

        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out
            self.game.readAttacks(self.attackconfig)
            output = out.getvalue().strip()
#            assert output == ".Blue laydown valid?: True\nRed laydown valid?: True\nBlue fleet all sunk. Red player wins!\nGame over\nFFFFFFF."
            assert output == "Blue laydown valid?: True\nRed laydown valid?: True\nRed Fired at: 10,5 with outcome: Hit\nBlue Fired at: 1,1 with outcome: Miss\nRed Fired at: 10,6 with outcome: Hit\nBlue Fired at: 1,2 with outcome: Miss\nRed Fired at: 10,7 with outcome: Hit\nBlue Fired at: 1,3 with outcome: Miss\nRed Fired at: 10,8 with outcome: Hit\nBlue Fired at: 1,4 with outcome: Miss\nRed Fired at: 10,9 with outcome: Sunk\nBlue fleet all sunk. Red player wins!\nGame over"
        finally:
            sys.stdout = saved_stdout

#    def test_validateOutsideBottomRight(self):
        # Assuming board is 10 x 10 starting from 0
        # Testing if attack is on the outside of the board in the bottom right corner
#        self.game.attackconfig="settings/attackConfigOutsideBottomRight.txt"
#        self.assertEqual(self.game.readAttacks(self.game.attackconfig), None)


if __name__=='__main__':
    unittest.main(argv=['first-arg-is-ignored', "GameTest"],exit=False)


